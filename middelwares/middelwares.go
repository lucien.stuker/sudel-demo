package middelwares

import (
	"strings"

	"github.com/labstack/echo-contrib/echoprometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// SetMiddlewares - Set generic middlewares
func SetMiddlewares(e *echo.Echo) {
	e.Use(serverHeader)

	// Enable echo logger middleware
	e.Use(middleware.Logger())

	// Enable echp recover middleware
	e.Use(middleware.Recover())

	// Enable echp recover metric middleware
	e.Use(echoprometheus.NewMiddleware("demoapp")) // adds middleware to gather metrics
}

// urlSkipper - ignores metrics toute on some middleware
func urlSkipper(c echo.Context) bool {
	if strings.HasPrefix(c.Path(), "/metrics") {
		return true
	}
	if strings.HasPrefix(c.Path(), "/readiness") {
		return true
	}
	if strings.HasPrefix(c.Path(), "/liveness") {
		return true
	}
	return false
}

func serverHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set("demo-app-version", "v1.0")
		return next(c)
	}
}
