package router

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo-contrib/echoprometheus"

	"gitlab.com/4data/sandbox/petit-echo/handlers"
	"gitlab.com/4data/sandbox/petit-echo/middelwares"
)

func New() *echo.Echo {
	
	// create a echo instance
	e := echo.New()

	middelwares.SetMiddlewares(e)

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Demo App!")
	})

	e.GET("/hello", func(c echo.Context) error {
		return c.String(http.StatusOK, "hello")
	})

	healthRoutes(e)

	e.GET("/metrics", echoprometheus.NewHandler()) // adds route to serve gathered metrics

	return e
}

func healthRoutes(e *echo.Echo) {
	e.GET("/liveness", handlers.GetLiveness)
	e.GET("/readiness", handlers.GetReadiness)
}