package handlers

import (
	"net/http"
	"github.com/labstack/echo/v4"
)

// type Health struct {
// 	Status string `json:"status"`
// 	Checks []Checks `json:"status"`
// }

type LivenessProbe struct {
	Status string `json:"status"`
}

type ReadinessProbe struct {
	Status string `json:"status"`
}

//GetReadiness - Health Check Handler
func GetReadiness(c echo.Context) error {
	resp := ReadinessProbe{Status: "ready"}

	return c.JSON(http.StatusOK, resp)
}

//GetLiveness - Health Check Handler
func GetLiveness(c echo.Context) error {
	resp := LivenessProbe{Status: "ok"}

	return c.JSON(http.StatusOK, resp)
}

