
# Cosign Demo

## Using HashiCorp Vault to Store Key

Set HashiCorp Vault address

```
export VAULT_ADDR=https://vault.local.devbot.io:8200
```

Set token to access HashuCorp Vault

```
export VAULT_TOKEN=$(jq -r ".root_token" ~/code/lucien/vault/cluster-keys.json)
```


Generate Signing Key pair and store them in HasiVault transit store

```
cosign generate-key-pair --kms hashivault://cosign-demo
```


## Build Container Image

Build Conatiner

```
docker image build --tag docker.io/4data/petit-echo:1.0.0 .
```


Push Container to docker hub

```
docker image push docker.io/4data/petit-echo:1.0.0
```

## Client-Side Container Image Validation With Cosign

```
cosign verify --key hashivault://cosign-demo \
    docker.io/4data/petit-echo:1.0.0
```

## Enforec Uage of Sinaed Container Images with Keyverno

```
kubectl create namespace production
```

Retrive Public key

```
cosign public-key --key hashivault://cosign-demo
```

```
yq --inplace \ 
    ".spec.rules[0].verifyImages[0].attestors[0].entries[0].keys.publicKeys = \"|- $(cosign public-key --key hashivault://cosign-demo)\"" \ 
    manifests/cosign/kyverno.yaml
```

Apply kyverno policy to check image
```
kubectl --namespace production apply \
    --filename manifests/cosign/kyverno.yaml
```

```
kubectl --namespace production apply \
    --filename manifests/
```

## Sign Container Images with Sigstore Cosign

```
cosign sign --key hashivault://cosign-demo \
    docker.io/4data/petit-echo:1.0.0
```

```
cosign verify --key hashivault://cosign-demo \
    docker.io/4data/petit-echo:1.0.0
```

```
kubectl --namespace production apply \
    --filename manifests/
```

## Destroy

```
kubectl --namespace production delete \
    --filename manifests/
```