package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"gitlab.com/4data/sandbox/petit-echo/router"
)

func main() {

	e := router.New()

	go func() {
		if err := e.Start(":1323"); err != nil {
			e.Logger.Info("shutting down the server")
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
// 	e := echo.New()
// 
// 	e.Use(middleware.Logger())
// 
// 	e.Use(echoprometheus.NewMiddleware("myapp")) // adds middleware to gather metrics
// 	e.GET("/metrics", echoprometheus.NewHandler()) // adds route to serve gathered metrics
// 
// 	e.GET("/", func(c echo.Context) error {
// 		return c.String(http.StatusOK, "Demo App!")
// 	})
// 
// 	e.GET("/hello", func(c echo.Context) error {
// 		return c.String(http.StatusOK, "hello")
// 	})
// 
// 	e.Logger.Fatal(e.Start(":1323"))
}