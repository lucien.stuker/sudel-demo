FROM golang:1.20.5 AS build
RUN mkdir -p /src/gitlab.com/4data/sandbox/petit-echo
WORKDIR /src/gitlab.com/4data/sandbox/petit-echo

COPY ./go.mod .
COPY ./go.sum .

RUN go mod download
RUN go mod verify

COPY . .
#RUN go get -d -v -t
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o petit-echo
RUN chmod +x petit-echo

FROM scratch
COPY --from=build /src/gitlab.com/4data/sandbox/petit-echo/petit-echo /usr/local/bin/petit-echo
EXPOSE 1323
CMD ["petit-echo"]